package com.zhiwei.redis.simulate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

/**
 * Jedis 客户端
 * @author  ZHIWEI.YANG
 * @time   2018年9月21日-下午8:54:57
 */
public class JedisClient {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private String host = "localhost";
    private Integer port = 6379;
    private Jedis jedis;

    @Before
    public void init() {
        jedis = new Jedis(host, port);
    }

    @Test
    public void jedisTest() {
		
		jedis.set("name", "zhangsan");
        logger.info(jedis.get("name"));
		jedis.close();
	}

    @After
    public void close() {
        if (null != jedis)
            jedis.close();
    }
}
