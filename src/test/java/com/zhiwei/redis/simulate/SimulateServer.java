package com.zhiwei.redis.simulate;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 模拟服务端:解析jedis发送的原始请求报文
 * @author  ZHIWEI.YANG
 * @time   2018年9月21日-下午8:49:25
 */
public class SimulateServer {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private Integer port = 6379;
    private Integer bufferSize = 1024;

    /**
     * 服务端模拟
     * @throws IOException
     */
    @Test
    public void server() throws IOException {

        ServerSocket serverSocket = new ServerSocket(port);
		Socket socket = serverSocket.accept();
        byte[] bytes = new byte[bufferSize];
		socket.getInputStream().read(bytes);

        logger.info("请求：{}", new String(bytes));
		serverSocket.close();
	}
}
