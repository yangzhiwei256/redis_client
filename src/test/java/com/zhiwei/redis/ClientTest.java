package com.zhiwei.redis;

import com.zhiwei.redis.client.RedisClient;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ClientTest {

    private Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void clientSetTest() throws IOException {

		RedisClient client = new RedisClient("localhost", 6379);
        logger.info("结果：{}", client.set("age", "10"));
	}

	@Test
	public void clientGetTest() throws IOException {

		RedisClient client = new RedisClient("localhost", 6379);
        logger.info("结果：{}", client.get("age"));
	}

	@Test
	public void clientKeysTest() throws IOException {

		RedisClient client = new RedisClient("localhost", 6379);
        logger.info("结果：{}", client.getAll());
	}
}
