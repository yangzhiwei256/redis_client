package com.zhiwei.redis.client;

import com.zhiwei.redis.protocol.RedisCommand;
import com.zhiwei.redis.transport.RedisConnection;

/**
 * redis自定义客户端
 * @author ZHIWEI.YANG
 * @time 2018年9月21日-下午8:45:25
 */
public class RedisClient {

	private RedisConnection connection;

	public RedisClient(String host, Integer port) {
		connection = new RedisConnection(host, port);
	}

    public Object set(String key, String value) {

        return connection.sendCommand(RedisCommand.SET, key.getBytes(), value.getBytes());
	}

    public Object get(String key) {

        return connection.sendCommand(RedisCommand.GET, key.getBytes());
	}

    public Object getAll() {

        return connection.sendCommand(RedisCommand.KEYS, "*".getBytes());
	}
	
	public void close(){
		connection.close();
	}
}
