package com.zhiwei.redis.protocol;

import com.zhiwei.redis.util.IOUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * RESP协议： Redis 官方协议
 * @author ZHIWEI.YANG
 * @time 2018年9月21日-下午8:56:37
 */
public class RedisProtocol {

	/**
	 * 发送数据（组装数据）
	 */
	public static void sendData(OutputStream os, RedisCommand command, byte[]... args) {

		StringBuilder sb = new StringBuilder();
        sb.append(RedisProtocolChar.ASTERISK_BYTE).append(args.length + 1).append(RedisProtocolChar.BLANK_STRING);
        sb.append(RedisProtocolChar.DOLLAR_BYTE).append(command.name().length()).append(RedisProtocolChar.BLANK_STRING);
        sb.append(command.name()).append(RedisProtocolChar.BLANK_STRING);

		for (final byte[] arg : args) {
            sb.append(RedisProtocolChar.DOLLAR_BYTE).append(arg.length).append(RedisProtocolChar.BLANK_STRING);
            sb.append(new String(arg)).append(RedisProtocolChar.BLANK_STRING);
		}
		try {
			os.write(sb.toString().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 发送数据（组装数据）
	 */
    public static Object parseData(InputStream is, RedisCommand command) {

        if (RedisCommand.GET.equals(command)) {
            return parseGetData(is);
        }

        if (RedisCommand.KEYS.equals(command)) {
            return parseKeysData(is);
        }

        if (RedisCommand.SET.equals(command)) {
            return parseSetData(is);
        }
        return null;
	}

	/**
	 * @param is
	 * @return
	 */
	private static Object parseSetData(InputStream is) {
		return new String(IOUtil.getData(is)).substring(1);

	}

	/**
	 * @param is
	 * @return
	 */
	private static Object parseKeysData(InputStream is) {

		String data = new String(IOUtil.getData(is));

        if (data.substring(0, 1).equals(RedisProtocolChar.ERROR_STRING)) {
            return data.substring(1);
        }

        String[] allKeys = data.split(RedisProtocolChar.BLANK_STRING);
        List<String> keys = new ArrayList<String>(allKeys.length - 1);

        for (String key : allKeys) {
            if (!key.contains(RedisProtocolChar.DOLLAR_BYTE) && !key.contains(RedisProtocolChar.ASTERISK_BYTE) && key.trim().length() != 0) {
                keys.add(key);
            }
        }
        return keys;
    }

    /**
     * 处理Get命令
     *
     * @param is
     * @return
     */
    private static Object parseGetData(InputStream is) {
        String data = new String(IOUtil.getData(is));
        return (!data.contains(RedisProtocolChar.ERROR_STRING))
                ? data.substring(data.indexOf(RedisProtocolChar.BLANK_STRING)).replace(RedisProtocolChar.BLANK_STRING, "").toString()
                : data;
    }
}
