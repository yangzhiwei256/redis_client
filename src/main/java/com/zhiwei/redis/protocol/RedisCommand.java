package com.zhiwei.redis.protocol;

/**
 * @author ZHIWEI.YANG
 * @createtime 2019/1/29 - 10:18
 * @decription 定义redis命令枚举值
 **/
public enum RedisCommand {
    GET(1, "获取key值"),
    SET(2, "设置key值"),
    KEYS(3, "获取所有key");

    public Integer code;
    public String msg;

    RedisCommand(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public String getMsg(Integer code) {
        for (RedisCommand redisCommand : RedisCommand.values()) {
            if (redisCommand.getCode().equals(code)) {
                return redisCommand.getMsg();
            }
        }
        return null;
    }
}
