package com.zhiwei.redis.protocol;

/**
 * @author ZHIWEI.YANG
 * @createtime 2019/1/29 - 10:21
 * @decription Redis 协议字符
 **/
public final class RedisProtocolChar {

    public static final String DOLLAR_BYTE = "$";
    public static final String ASTERISK_BYTE = "*";
    public static final String BLANK_STRING = "\r\n";
    public static final String ERROR_STRING = "-";
}
