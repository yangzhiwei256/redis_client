package com.zhiwei.redis.transport;

import com.zhiwei.redis.protocol.RedisCommand;
import com.zhiwei.redis.protocol.RedisProtocol;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Redis连接客户端
 * @author ZHIWEI.YANG
 * @time 2018年9月21日-下午8:43:19
 */
public class RedisConnection {

	private Socket socket;
	private String host;
	private Integer port;
	private OutputStream os;
	private InputStream is;
	private Boolean isActive = false;

	/**
	 * @param host
     * @param port
	 */
	public RedisConnection(String host, Integer port) {
		super();
		this.host = host;
		this.port = port;
	}

    private void connect() {

		try {
			socket = new Socket(host, port);
			is = socket.getInputStream();
			os = socket.getOutputStream();
			isActive = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 发送命令
	 * 
	 * @param command
	 * @param args
	 * @return
	 */
    public Object sendCommand(RedisCommand command, byte[]... args) {

		if (!isActive) {
			connect();
		}
		RedisProtocol.sendData(os, command, args);
        return RedisProtocol.parseData(is, command);
	}

	public void close() {
        
		if(!isActive) {
			return;
		}
		
		try {
			if (is != null) {
				is.close();
			}
			if (os != null) {
				os.close();
			}
			if (socket != null) {
				socket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		isActive = false;
	}
}
