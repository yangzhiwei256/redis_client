package com.zhiwei.redis.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 *  IO工具类
 * @author  ZHIWEI.YANG
 * @time   2018年9月21日-下午11:11:04
 */
public class IOUtil {

    private final static Logger logger = LoggerFactory.getLogger(IOUtil.class);

    /**
     * 缓存数据增长因子，默认1部
     */
    private static Integer factor = 1;

    /**
     * 从输入流获取数据
     *
     * @param is
     * @return
     */
	public  static byte[] getData(InputStream is) {
		
		byte[] result = new byte[1024];
		int count = 0;
		byte[] buf = new byte[1024];
		int length = 0;

        try {
            if ((length = is.read(buf)) != 0) {
                if (result.length < count + length) {
                    result = Arrays.copyOf(result, result.length << factor);
                }
                System.arraycopy(buf, 0, result, count, length);
                count += length;
            }
        } catch (IOException e) {
            logger.error("从输入流读取数据出错，请仔细排查问题， Exception:{}", e);
            return null;
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                logger.error("从输入流读取数据出错，请仔细排查问题， Exception:{}", e);
            }
        }
		return result;
	}
}
